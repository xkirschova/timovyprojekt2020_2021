Aplikácia umožňuje používateľom vyhľadávať rôzne firmy a vytvárať objednávky na tovar, ktorý ponúkajú. 

V aplikácii existujú viaceré typy používateľských účtov: zákazník, firma, kuriér, správca. 
- Zákazník si môže ukladať firmy do zoznamu obľúbených, zobraziť históriu objednávok, hodnotiť jednotlivé firmy a kuriérov. 
- Firma si môže podať žiadosť o pridanie novej pobočky, má k dispozícii manažment detailov a ponuky tovaru jednotlivých pobočiek. 
- Kuriér si vie podať žiadosť o zaregistrovanie, po schválení má k dispozícii manažment svojho profilu. 
- Správca môže schvaľovať žiadosti firiem a kuriérov.