<?php
$filename = $_GET["file"] .".pdf";
$file = "../pdf/".$filename;

header('Content-Type: application/pdf');
header('Content-Length: ' . filesize($file));
header('Content-Transfer-Encoding: BASE64');
header('Content-Disposition: inline; filename="'.$filename.'"');

readfile($file);
exit;
?>